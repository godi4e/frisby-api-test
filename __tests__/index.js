const frisby = require('frisby');

var fs = require('fs');
var path = require('path');
var FormData = require('form-data');

var keywords = new FormData();

var keywordsPath = path.resolve(__dirname, 'assets/keywords.csv');

keywords.append('isOverwrite', false);
keywords.append('adGroupId', 3978);
keywords.append('file', fs.createReadStream(keywordsPath), {
  knownLength: fs.statSync(keywordsPath).size
})

const login = function(){
	var form = new FormData();

	form.append('email', 'admin@dataswitch.com');
	form.append('password', 'admin')

	return frisby.post('http://aa-cm-dev.thumbtack.lo/ds-api/auth/login', {
		body: Promise.resolve(form),
		redirect: 'manual',
		headers: {
			'X-AUTH_AGENT':'REST_API',
			'Content-Type': 'application/x-www-form-urlencoded'
		}
	})
	.expect('status', 302)
}

it('should fail with valid keyword upload', function (done) {
	login()
	.then((response) => {
		var authCookie = response.headers._headers['set-cookie']

		return frisby.post('http://aa-cm-dev.thumbtack.lo/ds-api/proxy/keywords/upload', {
			body: Promise.resolve(keywords),
			headers: { 
				'Cookie': authCookie, 
				'Content-Type': 'multipart/form-data' 
			}
		})
		.expect('status', 500)
	})
	.done(done);

 });

it('should return keywords by filter query', function (done) {
	login()
	.then((response) => {
		var authCookie = response.headers._headers['set-cookie']

		return frisby.get('http://aa-cm-dev.thumbtack.lo/ds-api/proxy/keywords?filters[metricDate][operator]=>=&filters[metricDate][condition]=2017-08-06&filters[stateId][operator]==&filters[stateId][condition]=1&filters[timeFrame]=day&filters[adGroupId]=131&filters[name][operator]=~&filters[name][condition]=*test*&page=0&size=25&total_pages=1&total_entries=3&properties=impressions&sort=desc', {
			headers: { 
				'Cookie': authCookie
			}
		})
		.expect('status', 200)
	})
	.done(done);

 });


